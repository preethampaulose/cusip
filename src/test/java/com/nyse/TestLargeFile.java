package com.nyse;

import java.io.File;
import java.io.PrintWriter;

import org.junit.Test;

public class TestLargeFile {


    @Test
    public void readALargeFile_TakesTime() {
	String temporaryFileName = getTemporaryFileName();
	createALargeFile(temporaryFileName);
	
	CUSIPFile cFile = new CUSIPFile(temporaryFileName);
	cFile.read();
	
	delete(temporaryFileName);
    }

    private void delete(String temporaryFileName) {
	File file = new File (temporaryFileName);
	file.delete();	
    }

    private void createALargeFile(String temporaryFileName) {
	try (PrintWriter writer = new PrintWriter(temporaryFileName, "UTF-8");) {
	    for (int cusip = 0; cusip < 999999; cusip++) {
		writer.println(truncateToEightCharacter(String.valueOf(cusip) + "AAAAAAAA"));
		for (int price = 0; price < 9999; price++)
		    writer.println(price);
	    }
	} catch (Exception e) {
	    System.out.println("Error in creating temporary cusip file. ERROR : " + e.getMessage());
	}

    }

    private String truncateToEightCharacter(String value) {
	  if (value != null && value.length() > 8) {
	    value = value.substring(0, 8);
	  }
	  return value;
    }

    private String getTemporaryFileName() {
	return System.getProperty("user.dir") + File.separator + "DataExtract.txt";
    }

}
