package com.nyse;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestFileRead {

    private String temporaryFileName;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
	System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
	System.setOut(originalOut);
	System.out.println(outContent.toString());
    }

    @Test
    public void fileContainingOneCusipAndOnePrice() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("AAAA1234", "10.25");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("AAAA1234 : 10.25", outContent.toString().trim());
    }

    @Test
    public void fileContainingOneCusipAndNoPrice() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("AAAA1234");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("", outContent.toString().trim());
    }

    @Test
    public void emptyFile() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("", outContent.toString().trim());
    }

    @Test
    public void fileContainingNullValues() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile(null, "1.23", "1.55", "AAAA1234", "10.25", null);

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("AAAA1234 : 10.25", outContent.toString().trim());
    }

    @Test
    public void nonExistentFileWillReturnAErrorMessage() {

	CUSIPFile cFile = new CUSIPFile("NonExistentFile.txt");
	cFile.read();

	assertEquals(
		"Error in reading the CUSIP file NonExistentFile.txt ERROR : File 'NonExistentFile.txt' does not exist",
		outContent.toString().trim());
    }

    @Test
    public void fileContainingBlankPrices() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("AAAA1234", "", "", " ");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("", outContent.toString().trim());
    }

    @Test
    public void fileContainingOneCusip() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("AAAA1234", "10.45", "10.55", "11.25");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("AAAA1234 : 11.25", outContent.toString().trim());
    }

    @Test
    public void fileContainingTwoCusips() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("AAAA1234", "10.45", "10.55", "11.25",

		"AAAA1222", "99.45", "99.55", "99.25");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("AAAA1234 : 11.25" + System.lineSeparator() + "AAAA1222 : 99.25", outContent.toString().trim());
    }

    @Test
    public void threeCUSIPsWithOneHavingNoPrice() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("AAAA1234", "10.45", "10.55", "11.25",

		"BBBB2222",

		"AAAA1222", "99.45", "99.55", "99.25");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("AAAA1234 : 11.25" + System.lineSeparator() + "AAAA1222 : 99.25", outContent.toString().trim());
    }

    @Test
    public void fileStartingWithPrice() {
	this.temporaryFileName = getTemporaryFileName();
	createCusipFile("10.45", "10.55", "11.25",

		"AAAA1234", "10.45", "10.55", "11.25",

		"AAAA1222", "99.45", "99.55", "99.25");

	CUSIPFile cFile = new CUSIPFile(this.temporaryFileName);
	cFile.read();

	assertEquals("AAAA1234 : 11.25" + System.lineSeparator() + "AAAA1222 : 99.25", outContent.toString().trim());
    }

    private void createCusipFile(String... lines) {
	try (PrintWriter writer = new PrintWriter(this.temporaryFileName, "UTF-8");) {
	    for (String line : lines)
		writer.println(line);
	} catch (Exception e) {
	    System.out.println("Error in creating temporary cusip file. ERROR : " + e.getMessage());
	}

    }

    private String getTemporaryFileName() {
	return System.getProperty("user.dir") + File.separator + "DataExtract.txt";
    }

}
