package com.nyse;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class CUSIPFile {

    private String fileName;
    private LineIterator iterator;

    public CUSIPFile(String fileName) {
	this.fileName = fileName;
    }

    public void read() {
	try {
	    this.iterator = FileUtils.lineIterator(new File(this.fileName), "UTF-8");
	    String cusip = "";
	    String price = "";
	    while (this.iterator.hasNext()) {
		String line = this.iterator.nextLine();
		if (isAPrice(line))
		    price = line;
		else if (isACusip(line)) {
		    printCusipAndPrice(cusip, price);
		    cusip = line;
		    price = "";
		}
	    }
	    // Print the last CUSIP and Price
	    printCusipAndPrice(cusip, price);
	} catch (IOException e) {
	    System.out.println("Error in reading the CUSIP file " + this.fileName + " ERROR : " + e.getMessage());
	} finally {
	    try {
		if (this.iterator != null)
		    this.iterator.close();
	    } catch (IOException e) {
		System.out.println("Error in closing the CUSIP file " + this.fileName + " ERROR : " + e.getMessage());
	    }
	}
    }

    private void printCusipAndPrice(String cusip, String price) {
	if (cusip != "" && price != "")
	    System.out.println(cusip + " : " + price);
    }

    private boolean isACusip(String line) {
	return line.matches("^[A-Za-z0-9]{8}$");
    }

    private boolean isAPrice(String line) {
	return line.matches("^[0-9.]+?$");
    }

}
